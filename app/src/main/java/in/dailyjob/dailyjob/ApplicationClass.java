package in.dailyjob.dailyjob;

import android.app.Application;

/**
 * Created by ramakant on 25/6/17.
 */

public class ApplicationClass extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        GoogleClient.getGoogleApiClient(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        GoogleClient.getGoogleApiClient(getApplicationContext()).disconnect();
    }
}
