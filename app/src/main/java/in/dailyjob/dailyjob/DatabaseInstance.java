package in.dailyjob.dailyjob;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;

import in.dailyjob.dailyjob.Interface.AddPostListener;
import in.dailyjob.dailyjob.Interface.GetUserPostsListener;
import in.dailyjob.dailyjob.model.AddNewItemModel;

/**
 * Created by ramakant on 26/6/17.
 */

public class DatabaseInstance {

    public static String TAG = DatabaseInstance.class.getSimpleName();
    private static FirebaseDatabase firebaseInstance;

    private DatabaseInstance(){}

    private static FirebaseDatabase createInstance(){
        if(firebaseInstance == null){
            firebaseInstance = FirebaseDatabase.getInstance();
            firebaseInstance.setPersistenceEnabled(true);
        }
        return firebaseInstance;
    }
    public static FirebaseDatabase getFirebaseInstance(){
        return createInstance();
    }

    public static void addNewPost(final AddPostListener addPostListener, final AddNewItemModel addNewItemModel){
        FirebaseDatabase firebaseDatabase = getFirebaseInstance();
        firebaseDatabase.getReference().child(Constants.FIREBASE_LOCALITY_TABLE_NAME).child(addNewItemModel.getLocality()).child(addNewItemModel.getPostId()).setValue(addNewItemModel).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.v(TAG,"New Item Added Successfully");
                //addPostListener.AddPostListenerResult(Constants.SUCCESS_RESULT);
                addNewPostToUserTable(addPostListener,addNewItemModel);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.v(TAG,"New Item Not Added");
                addPostListener.AddPostListenerResult(Constants.FAILURE_RESULT);
            }
        });
    }

    public static void addNewPostToUserTable(final AddPostListener addPostListener, AddNewItemModel addNewItemModel){
        FirebaseDatabase firebaseDatabase = getFirebaseInstance();
        firebaseDatabase.getReference().child(Constants.FIREBASE_USER_TABLE_NAME).child(addNewItemModel.getUserId()).child(addNewItemModel.getLocality()).child(addNewItemModel.getPostId()).setValue(addNewItemModel).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.v(TAG,"New Item Added Successfully");
                addPostListener.AddPostListenerResult(Constants.SUCCESS_RESULT);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.v(TAG,"New Item Not Added");
                addPostListener.AddPostListenerResult(Constants.FAILURE_RESULT);
            }
        });
    }


    public static void getUserData(final GetUserPostsListener userPostsListener) {
        FirebaseDatabase firebaseDatabase = getFirebaseInstance();
        final ArrayList<AddNewItemModel> userData = new ArrayList<>();
        firebaseDatabase.getReference().child(Constants.FIREBASE_USER_TABLE_NAME).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).getRef().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                //dataSnapshot.getChildren().iterator().next().getChildren().iterator().next().getValue()
                for (DataSnapshot uniqueUserSnapshot : dataSnapshot.getChildren()) {
                    for (DataSnapshot snapshot : uniqueUserSnapshot.getChildren()){
                        AddNewItemModel post = snapshot.getValue(AddNewItemModel.class);
                        userData.add(post);
                    }
                }

                Log.v(TAG, "user data");
                userPostsListener.userPosts(userData);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
