package in.dailyjob.dailyjob;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;

/**
 * Created by ramakant on 24/6/17.
 */

public class UserDetailsPreference {

    public static SharedPreferences userDetailsPreference;

    public static String U_NAME = "U_NAME";
    public static String U_PROFILEPIC = "U_PROFILEPIC";
    public static String U_EMAIL = "U_EMAIL";


    public static SharedPreferences getUserDetailsPreference(Context context){
        if(userDetailsPreference == null)
            userDetailsPreference = context.getSharedPreferences("DailyJob",Context.MODE_PRIVATE);
        return userDetailsPreference;
    }
}
