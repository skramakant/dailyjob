package in.dailyjob.dailyjob;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

import in.dailyjob.dailyjob.Interface.GetUserPostsListener;
import in.dailyjob.dailyjob.model.AddNewItemModel;

/**
 * Created by ramakant on 25/6/17.
 */

public class ShowItemDetailsBottomSheet extends BottomSheetDialogFragment implements GetUserPostsListener{
    private static final int REQUEST_CODE_ADD_POST = 707;
    private RecyclerView mRecyclerView;
    private Button btAddPost;
    private View rootView;
    private LinearLayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private ArrayList<AddNewItemModel> myDataset;
    private GetUserPostsListener userPostsListener;
    private LatLng mLastLocation;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layouy_user_posts, container, false);
        intializeView();
        return rootView;
    }

    private void intializeView() {
        if(getArguments() != null){
             mLastLocation = getArguments().getParcelable("MarkerLocation");
        }
        btAddPost = (Button) rootView.findViewById(R.id.btAddPost);
        btAddPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),AddNewItem.class);
                intent.putExtra("MarkerLocation",mLastLocation);
                startActivityForResult(intent,REQUEST_CODE_ADD_POST);
            }
        });

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.rvUserPosts);
        //mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(false);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        myDataset = new ArrayList<>();
        //mAdapter = new MyAdapter(myDataset);
        //mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            switch (requestCode){
                case REQUEST_CODE_ADD_POST:
                    Toast.makeText(getActivity(), "Post Added Successfully", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        userPostsListener = this;
        getUserPosts();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
        userPostsListener = null;
    }

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    public void getUserPosts() {
        if(userPostsListener != null)
            DatabaseInstance.getUserData(userPostsListener);
    }

    @Override
    public void userPosts(ArrayList<AddNewItemModel> userPosts) {
        myDataset = userPosts;
        mAdapter = new MyAdapter(myDataset);
        mRecyclerView.setAdapter(mAdapter);
    }


    public static class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
        private ArrayList<AddNewItemModel> mDataset;

        public static class ViewHolder extends RecyclerView.ViewHolder {
            // each data item is just a string in this case
            public TextView tvPostTitle;
            public TextView tvPostDetails;
            public ViewHolder(View v) {
                super(v);
                tvPostTitle = (TextView) v.findViewById(R.id.tvPostTitle);
                tvPostDetails = (TextView) v.findViewById(R.id.tvPostDetails);
            }
        }

        public MyAdapter(ArrayList<AddNewItemModel> myDataset) {
            mDataset = myDataset;
        }

        // Create new views (invoked by the layout manager)
        @Override
        public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_user_post_item, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.tvPostTitle.setText(mDataset.get(position).getPostTitle());
            holder.tvPostDetails.setText(mDataset.get(position).getPostDiscription());
        }
        @Override
        public int getItemCount() {
            return mDataset.size();
        }

    }


}
