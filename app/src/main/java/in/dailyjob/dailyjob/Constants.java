package in.dailyjob.dailyjob;

/**
 * Created by ramakant on 26/6/17.
 */

public class Constants {

    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;
    public static final String PACKAGE_NAME =
            "in.dailyjob.dailyjob";
    public static final String RECEIVER = PACKAGE_NAME + ".AddNewItem.AddressResultReceiver";
    public static final String RESULT_DATA_KEY = PACKAGE_NAME +
            ".RESULT_DATA_KEY";
    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME +
            ".LOCATION_DATA_EXTRA";


    //Post Category is declared hare

    public static int RENT = 1;
    public static int SEARCHING_WORK = 2;
    public static int SEARCHING_WORKERS =3;
    public static int TUTORS = 4;
    public static int BUY = 5;
    public static int SELL = 6;

    //FIREBASE TABLE NAME
    public static String FIREBASE_LOCALITY_TABLE_NAME = "LOCALITY";
    public static String FIREBASE_USER_TABLE_NAME = "USERS";
}
