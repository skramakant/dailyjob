package in.dailyjob.dailyjob.Interface;

import java.util.ArrayList;

import in.dailyjob.dailyjob.model.AddNewItemModel;

/**
 * Created by ramakant on 29/7/17.
 */

public interface GetUserPostsListener {

    public void userPosts(ArrayList<AddNewItemModel> userPosts);
}
