package in.dailyjob.dailyjob;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Address;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.UUID;

import in.dailyjob.dailyjob.Interface.AddPostListener;
import in.dailyjob.dailyjob.model.AddNewItemModel;

public class AddNewItem extends AppCompatActivity implements AddPostListener {

    private AddressResultReceiver mResultReceiver;
    private LatLng mLastLocation;
    private Spinner catSpinner;
    private TextView tvAddress;
    private EditText etPersonName;
    private EditText etPersonMobile;
    private EditText etPersonEmail;
    private EditText etTitle;
    private EditText etAmount;
    private EditText etDetails;
    private int selectedCategeoryId ;
    private String selectedLocality;
    private String selectedAddress;
    private AddPostListener addPostListener;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_item);
        initializeView();
        initializeProgressDialog();
        setUpCatSpinner();
        if(getIntent().getExtras() != null){
            mLastLocation = getIntent().getExtras().getParcelable("MarkerLocation");
            startIntentService();
        }
    }

    private void initializeProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please Wait");
    }


    public void postAddOnServer(View view){
        FirebaseDatabase firebaseDatabase = DatabaseInstance.getFirebaseInstance();

        AddNewItemModel addNewItemModel = new AddNewItemModel();

        addNewItemModel.setUserId(FirebaseAuth.getInstance().getCurrentUser().getUid());

        addNewItemModel.setLatitude(mLastLocation.latitude);
        addNewItemModel.setLongitude(mLastLocation.longitude);

        addNewItemModel.setCategeory(getSelectedCategory());
        addNewItemModel.setLocality(getSelectedLocality());
        addNewItemModel.setPostOwnerAddress(getSelectedAddress());

        addNewItemModel.setPostTitle(etTitle.getText().toString());
        addNewItemModel.setPostChargePerDay(etAmount.getText().toString());
        addNewItemModel.setPostDiscription(etDetails.getText().toString());

        addNewItemModel.setPostOwner(etPersonName.getText().toString());
        addNewItemModel.setPostOwnerMobileNumber(etPersonMobile.getText().toString());
        addNewItemModel.setPostOwnerEmail(etPersonEmail.getText().toString());

        addNewItemModel.setTimeStamp(System.currentTimeMillis());
        addNewItemModel.setPostId(UUID.randomUUID().toString());

        if(addPostListener != null){
            progressDialog.show();
            DatabaseInstance.addNewPost(addPostListener,addNewItemModel);

        }
    }

    private void initializeView() {
        tvAddress = (TextView) findViewById(R.id.tvAddress);
        etPersonName = (EditText) findViewById(R.id.etPersonName);
        etPersonMobile = (EditText) findViewById(R.id.etPersonMobile);
        etPersonEmail = (EditText) findViewById(R.id.etPersonEmail);
        etTitle = (EditText) findViewById(R.id.etTitle);
        etAmount = (EditText) findViewById(R.id.etAmount);
        etDetails = (EditText) findViewById(R.id.etDetails);
    }

    private void setUpCatSpinner() {
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> catAdapter = ArrayAdapter.createFromResource(this,
                R.array.category, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        catAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        catSpinner = (Spinner) findViewById(R.id.catSpinner);
        catSpinner.setAdapter(catAdapter);

        catSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedCat = (String) adapterView.getItemAtPosition(i);
                setSelectedCategoryId(i);
                Toast.makeText(AddNewItem.this, selectedCat, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    protected void startIntentService() {
        mResultReceiver = new AddressResultReceiver(new Handler());

        Intent intent = new Intent(this, FetchAddressIntentService.class);
        intent.putExtra(Constants.RECEIVER, mResultReceiver);
        intent.putExtra(Constants.LOCATION_DATA_EXTRA, mLastLocation);
        startService(intent);
    }

    public String getSelectedLocality() {
        return selectedLocality;
    }

    public void setSelectedLocality(String selectedLocality) {
        this.selectedLocality = selectedLocality;
    }

    public String getSelectedAddress() {
        return selectedAddress;
    }

    public void setSelectedAddress(String selectedAddress) {
        this.selectedAddress = selectedAddress;
    }
    public int getSelectedCategory() {
        return selectedCategeoryId+1;
    }

    public void setSelectedCategoryId(int selectedCategeory) {
        this.selectedCategeoryId = selectedCategeory;
    }

    @Override
    public void AddPostListenerResult(int successResult) {
        if(successResult == Constants.SUCCESS_RESULT){
            //Toast.makeText(this,"Post Added Successfully!",Toast.LENGTH_LONG);
            if(progressDialog.isShowing()) progressDialog.dismiss();
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK,returnIntent);
            finish();
        }else if(successResult == Constants.FAILURE_RESULT){
            Toast.makeText(this,"Please Try After Some Time",Toast.LENGTH_LONG);
        }
    }

    public class AddressResultReceiver extends android.os.ResultReceiver {
        /**
         * Create a new ResultReceive to receive results.  Your
         * {@link #onReceiveResult} method will be called from the thread running
         * <var>handler</var> if given, or from an arbitrary thread if null.
         *
         * @param handler
         */
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            //super.onReceiveResult(resultCode, resultData);
            if (resultCode == Constants.SUCCESS_RESULT) {

                Address address = resultData.getParcelable(Constants.RESULT_DATA_KEY);
                String locality = address.getLocality();
                setSelectedLocality(locality);
                ArrayList<String> addressFragments = new ArrayList<String>();
                for(int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                    addressFragments.add(address.getAddressLine(i));
                }

                String result = TextUtils.join(System.getProperty("line.separator"),addressFragments);
                tvAddress.setText(result);
                setSelectedAddress(result);

            }

        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        addPostListener = this;
    }

    @Override
    protected void onStop() {
        super.onStop();
        addPostListener = null;
    }
}
