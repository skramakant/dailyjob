package in.dailyjob.dailyjob.model;

/**
 * Created by ramakant on 2/7/17.
 */

public class AddNewItemModel {

    private String userId;
    private double timeStamp;
    private double latitude;
    private double longitude;
    private String locality;
    private String postId;


    private int categeory;
    private String postTitle;
    private String postChargePerDay;
    private String postDiscription;

    private String postOwner;
    private String postOwnerMobileNumber;
    private String postOwnerEmail;
    private String postOwnerAddress;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public double getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(double timeStamp) {
        this.timeStamp = timeStamp;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public int getCategeory() {
        return categeory;
    }

    public void setCategeory(int categeory) {
        this.categeory = categeory;
    }

    public String getPostChargePerDay() {
        return postChargePerDay;
    }

    public void setPostChargePerDay(String postChargePerDay) {
        this.postChargePerDay = postChargePerDay;
    }

    public String getPostDiscription() {
        return postDiscription;
    }

    public void setPostDiscription(String postDiscription) {
        this.postDiscription = postDiscription;
    }

    public String getPostOwner() {
        return postOwner;
    }

    public void setPostOwner(String postOwner) {
        this.postOwner = postOwner;
    }

    public String getPostOwnerMobileNumber() {
        return postOwnerMobileNumber;
    }

    public void setPostOwnerMobileNumber(String postOwnerMobileNumber) {
        this.postOwnerMobileNumber = postOwnerMobileNumber;
    }

    public String getPostOwnerEmail() {
        return postOwnerEmail;
    }

    public void setPostOwnerEmail(String postOwnerEmail) {
        this.postOwnerEmail = postOwnerEmail;
    }

    public String getPostOwnerAddress() {
        return postOwnerAddress;
    }

    public void setPostOwnerAddress(String postOwnerAddress) {
        this.postOwnerAddress = postOwnerAddress;
    }

}
