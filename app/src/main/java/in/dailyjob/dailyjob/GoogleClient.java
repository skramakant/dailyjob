package in.dailyjob.dailyjob;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;

/**
 * Created by ramakant on 25/6/17.
 */

public class GoogleClient {

    private static GoogleApiClient googleApiClient;
    private GoogleClient(){

    }

    public static GoogleApiClient createGoogleClient(Context context){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(context.getString(R.string.web_client_id))
                .requestEmail()
                .build();
        return new GoogleApiClient
                .Builder(context)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }

    public static GoogleApiClient getGoogleApiClient(Context context){
        if(googleApiClient == null){
            return createGoogleClient(context);
        }
        return googleApiClient;
    }
}
